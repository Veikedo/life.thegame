﻿using System;
using Life.Logic;
using Life.Logic.Contracts;

namespace Life.CLI
{
  internal class Program
  {
    private static void Main()
    {
      const int size = 3;
      var game = new Game(size, size);

      game.SetAlive(0, 1);
      game.SetAlive(1, 1);
      game.SetAlive(1, 2);

      PrintField(game.CurrentGameField);

      IGameState gameState = game.Step();
      gameState = game.Step();
      gameState = game.Step();
      gameState = game.Step();
      gameState = game.Step();

      PrintField(gameState.GameField);

      game.Stop();
      Console.ReadLine();
    }

    private static void PrintField(IGameField gameField)
    {
      for (int i = 0; i < gameField.Height; i++)
      {
        for (int j = 0; j < gameField.Width; j++)
        {
          Console.Write("{0} ", gameField[i, j] ? 1 : 0);
        }

        Console.WriteLine();
      }

      Console.WriteLine();
    }
  }
}