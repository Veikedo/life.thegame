﻿using System;
using Life.Logic;
using Life.Logic.Contracts;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Life.Tests
{
  [TestClass]
  public class LogicTests
  {
    [TestMethod]
    public void Test1()
    {
      var game = new Game(3, 3);
      
      game.SetAlive(2, 0);
      game.SetAlive(0,1);
      game.SetAlive(1,1);
      game.SetAlive(1,2);

      PrintField(game.CurrentGameField);

      PrintField(game.Step().GameField);
      PrintField(game.Step().GameField);
      PrintField(game.Step().GameField);
      PrintField(game.Step().GameField);
      PrintField(game.Step().GameField);
    }

    private static void PrintField(IGameField gameField)
    {
      for (int i = 0; i < gameField.Height; i++)
      {
        for (int j = 0; j < gameField.Width; j++)
        {
          Console.Write("{0} ", gameField[i, j] ? 1 : 0);
        }

        Console.WriteLine();
      }

      Console.WriteLine();
    }
  }
}