﻿using System;
using System.Collections.Generic;
using Life.Logic.Contracts;
using Life.Logic.Helpers;

namespace Life.Logic
{
  internal class GameStep : IGameStep
  {
    private readonly ICollection<Cell> _checkNextStep;
    private readonly ICollection<Cell> _differences;

    public GameStep(int fieldHeight, int fieldWidth)
      : this(Factory.GameField(fieldHeight, fieldWidth))
    {
    }

    public GameStep(IGameField gameField)
    {
      GameField = gameField;

      _checkNextStep = new ConcurrentHashSet<Cell>(Cell.Comparer);
      _differences = new ConcurrentHashSet<Cell>(Cell.Comparer);
    }

    public IGameField GameField { get; private set; }

    public IEnumerable<Cell> Differences
    {
      get { return _differences; }
    }

    public void SetAlive(int y, int x)
    {
      if (GameField.IsDead(y, x))
      {
        GameField[y, x] = true;
        _differences.Add(Factory.Cell(y, x, alive: true));

        AddSuspectCellAroundPoint(y, x);
      }
    }

    public void SetDead(int y, int x)
    {
      if (GameField.IsAlive(y, x))
      {
        GameField[y, x] = false;
        _differences.Add(Factory.Cell(y, x, alive: false));

        AddSuspectCellAroundPoint(y, x);
      }
    }

    public IEnumerable<Cell> CheckNextStep
    {
      get { return _checkNextStep; }
    }

    #region Helpers

    private void AddSuspectCellAroundPoint(int y, int x)
    {
      int startRow = Math.Max(y - 1, 0);
      int endRow = Math.Min(y + 2, GameField.Height);

      int startCol = Math.Max(x - 1, 0);
      int endCol = Math.Min(x + 2, GameField.Width);

      for (int i = startRow; i < endRow; i++)
      {
        for (int j = startCol; j < endCol; j++)
        {
          var cell = Factory.Cell(i, j, GameField.IsAlive(i, j));
          _checkNextStep.Add(cell);
        }
      }
    }

    #endregion
  }
}