﻿using System;
using Life.Logic.Contracts;

namespace Life.Logic.Helpers
{
  internal static class Factory
  {
    public static IGameStateCalculator GameStateCalculator()
    {
      return new GameStateCalculator();
    }

    public static IGameField GameField(int height, int width)
    {
      return new GameField(height, width);
    }

    public static Cell Cell(int y, int x, bool alive)
    {
      return new Cell(y, x, alive);
    }

    public static IGameStep GameStep(int fieldHeight, int fieldWidth)
    {
      return new GameStep(fieldHeight, fieldWidth);
    }

    public static IGameStep GameStep(IGameField field)
    {
      return new GameStep(field);
    }
  }
}