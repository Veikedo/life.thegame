﻿using System;
using System.Collections.Generic;
using System.Threading;

namespace Life.Logic.Helpers
{
  public class ProducerConsumer<T> where T : class
  {
    private readonly object _mutex = new object();
    private readonly Queue<T> _queue = new Queue<T>();
    private bool _isDead;

    public int Count
    {
      get { return _queue.Count; }
    }

    public void Enqueue(T task)
    {
      if (task == null)
      {
        throw new ArgumentNullException("task");
      }
      lock (_mutex)
      {
        if (_isDead)
        {
          throw new InvalidOperationException("Queue already stopped");
        }
        _queue.Enqueue(task);
        Monitor.Pulse(_mutex);
      }
    }

    public T Dequeue()
    {
      lock (_mutex)
      {
        while (_queue.Count == 0 && !_isDead)
        {
          Monitor.Wait(_mutex);
        }

        if (_queue.Count == 0)
        {
          return null;
        }

        return _queue.Dequeue();
      }
    }

    public void Stop()
    {
      lock (_mutex)
      {
        _isDead = true;
        Monitor.PulseAll(_mutex);
      }
    }
  }
}