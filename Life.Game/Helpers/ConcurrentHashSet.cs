﻿using System;
using System.Collections;
using System.Collections.Concurrent;
using System.Collections.Generic;

namespace Life.Logic.Helpers
{
  public class ConcurrentHashSet<T> : ICollection<T>
  {
    private readonly ConcurrentDictionary<T, byte> _set;

    public ConcurrentHashSet(IEqualityComparer<T> equalityComparer = null)
    {
      var cmp = equalityComparer ?? EqualityComparer<T>.Default;
      _set = new ConcurrentDictionary<T, byte>(cmp);
    }

    public void Add(T value)
    {
      // TODO исправить костыль
      if (_set.ContainsKey(value))
      {
        Remove(value);
      }

      _set.GetOrAdd(value, 0);
    }

    public void Clear()
    {
      _set.Clear();
    }

    public bool Contains(T item)
    {
      return _set.ContainsKey(item);
    }

    public void CopyTo(T[] array, int arrayIndex)
    {
      throw new NotImplementedException();
    }

    public bool Remove(T item)
    {
      byte b;
      return _set.TryRemove(item, out b);
    }

    public int Count
    {
      get { return _set.Count; }
    }

    public bool IsReadOnly
    {
      get { return ((ICollection<KeyValuePair<T, byte>>) _set).IsReadOnly; }
    }

    public IEnumerator<T> GetEnumerator()
    {
      return _set.Keys.GetEnumerator();
    }

    IEnumerator IEnumerable.GetEnumerator()
    {
      return GetEnumerator();
    }
  }
}