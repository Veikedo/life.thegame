﻿using System.Collections.Generic;

namespace Life.Logic
{
  public class Cell
  {
    public Cell(int y, int x, bool alive)
    {
      X = x;
      Y = y;
      Alive = alive;
    }

    #region XYEqualityComparer

    private static readonly IEqualityComparer<Cell> ComparerInstance = new EqualityComparer();

    public static IEqualityComparer<Cell> Comparer
    {
      get { return ComparerInstance; }
    }

    private sealed class EqualityComparer : IEqualityComparer<Cell>
    {
      public bool Equals(Cell x, Cell y)
      {
        return x.X == y.X && x.Y == y.Y;
      }

      public int GetHashCode(Cell obj)
      {
        unchecked
        {
          return (obj.X*397) ^ obj.Y;
        }
      }
    }

    #endregion

    public int X { get; private set; }
    public int Y { get; private set; }
    public bool Alive { get; private set; }
  }
}