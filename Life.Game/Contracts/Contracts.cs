﻿using System;
using System.Collections.Generic;

namespace Life.Logic.Contracts
{
  public interface IGame : IGameFieldManager
  {
    IGameField CurrentGameField { get; }
    int CurrentGeneration { get; }
    IGameState Step();
    void Stop();
  }

  public interface IGameFieldManager
  {
    void SetDead(int y, int x);
    void SetAlive(int y, int x);
  }

  public interface IGameField
  {
    int Width { get; }
    int Height { get; }
    bool this[int y, int x] { get; set; }
    bool IsAlive(int y, int x);
    bool IsDead(int y, int x);
    int AliveCellsAroundPoint(int y, int x);
    IGameField Clone();
  }

  public interface IGameState
  {
    IGameField GameField { get; }

    /// <summary>
    ///   Diff from prev step
    /// </summary>
    IEnumerable<Cell> Differences { get; }
  }

  public interface IGameStep : IGameState, IGameFieldManager
  {
    /// <summary>
    ///   Suspected cells
    /// </summary>
    IEnumerable<Cell> CheckNextStep { get; }
  }

  internal interface IGameStateCalculator : IDisposable
  {
    IGameStep CalculateNextStep(IGameStep gameStep);
  }
}