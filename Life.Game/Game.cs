﻿using Life.Logic.Contracts;
using Life.Logic.Helpers;

namespace Life.Logic
{
  public class Game : IGame
  {
    private readonly IGameStateCalculator _gameStateCalculator;
    private IGameStep _currentGameStep;

    public Game(int fieldHeight, int fieldWidth)
    {
      _gameStateCalculator = Factory.GameStateCalculator();
      _currentGameStep = Factory.GameStep(fieldHeight, fieldWidth);
    }

    public IGameField CurrentGameField
    {
      get { return _currentGameStep.GameField; }
    }

    public int CurrentGeneration { get; private set; }

    public IGameState Step()
    {
      IGameStep nextStep = _gameStateCalculator.CalculateNextStep(_currentGameStep);
      CurrentGeneration++;
      return _currentGameStep = nextStep;
    }

    public void Stop()
    {
      _gameStateCalculator.Dispose();
    }

    public void SetDead(int y, int x)
    {
      _currentGameStep.SetDead(y, x);
    }

    public void SetAlive(int y, int x)
    {
      _currentGameStep.SetAlive(y, x);
    }
  }
}