﻿using System;
using Life.Logic.Contracts;

namespace Life.Logic
{
  internal class GameField : IGameField
  {
    private readonly bool[,] _field;

    public GameField(int height, int width)
    {
      Height = height;
      Width = width;

      _field = new bool[height, width];
    }

    private GameField(GameField gameField)
    {
      Height = gameField.Height;
      Width = gameField.Width;

      _field = (bool[,]) gameField._field.Clone();
    }

    public int Width { get; private set; }
    public int Height { get; private set; }

    public bool this[int y, int x]
    {
      get { return _field[y, x]; }
      set { _field[y, x] = value; }
    }

    public bool IsAlive(int y, int x)
    {
      return _field[y, x];
    }

    public bool IsDead(int y, int x)
    {
      return !IsAlive(y, x);
    }

    public int AliveCellsAroundPoint(int y, int x)
    {
      int count = 0;

      int startCol = Math.Max(x - 1, 0);
      int startRow = Math.Max(y - 1, 0);
      int endCol = Math.Min(x + 2, Width);
      int endRow = Math.Min(y + 2, Height);

      for (int i = startRow; i < endRow; i++)
      {
        for (int j = startCol; j < endCol; j++)
        {
          if (!(i == y && j == x) &&_field[i, j])
          {
            count++;
          }
        }
      }

      return count;
    }

    public IGameField Clone()
    {
      return new GameField(this);
    }
  }
}