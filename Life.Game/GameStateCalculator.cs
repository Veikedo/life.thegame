﻿using System.Threading.Tasks;
using Life.Logic.Contracts;
using Life.Logic.Helpers;

namespace Life.Logic
{
  internal class GameStateCalculator : IGameStateCalculator
  {
    private IGameField _currentGameField;
    private IGameStep _nextGameStep;

    public IGameStep CalculateNextStep(IGameStep gameStep)
    {
      _currentGameField = gameStep.GameField;
      _nextGameStep = Factory.GameStep(_currentGameField.Clone());

      /*foreach (var cell in gameStep.CheckNextStep)
      {
        Handle(cell);
      }

      return _nextGameStep;*/
      
      Parallel.ForEach(gameStep.CheckNextStep, Handle);
      return _nextGameStep;
    }

    private void Handle(Cell cell)
    {
      int num = _currentGameField.AliveCellsAroundPoint(cell.Y, cell.X);
      if (cell.Alive && (num > 3 || num <= 1))
      {
        _nextGameStep.SetDead(cell.Y, cell.X);
      }
      else if (num == 3)
      {
        _nextGameStep.SetAlive(cell.Y, cell.X);
      }
    }
  }
}