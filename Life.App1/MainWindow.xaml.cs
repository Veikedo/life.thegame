﻿using System.ComponentModel;
using System.Windows;
using System.Windows.Controls;
using Life.Logic;
using Life.Logic.Contracts;

namespace Life.App1
{
  public partial class MainWindow : Window
  {
    private Cell[,] _cells;
    private IGame _game;

    public MainWindow()
    {
      InitializeComponent();
    }

    private void SetButtonClick(object sender, RoutedEventArgs e)
    {
      int height = int.Parse(FieldHeight.Text);
      int width = int.Parse(FieldWidth.Text);

      InitGame(height, width);
    }

    private void InitGame(int fieldHeight, int fieldWidth)
    {
      GenerationTextBlock.Text = "0";

      _game = new Game(fieldHeight, fieldWidth);
      _cells = new Cell[fieldHeight, fieldWidth];

      var field = new Grid();

      double cellWidth = GameFieldGrid.ActualWidth/fieldWidth;

      for (int i = 0; i < fieldHeight; i++)
      {
        field.RowDefinitions.Add(new RowDefinition());

        for (int j = 0; j < fieldWidth; j++)
        {
          field.ColumnDefinitions.Add(new ColumnDefinition {Width = new GridLength(cellWidth)});

          var cell = _cells[i, j] = new Cell {Y = i, X = j};
          cell.SetValue(Grid.RowProperty, i);
          cell.SetValue(Grid.ColumnProperty, j);

          cell.CellStateChanged += OnCellStateChanged;

          field.Children.Add(cell);
        }
      }

      GameFieldGrid.Children.Clear();
      GameFieldGrid.Children.Add(field);
    }

    private void OnCellStateChanged(object sender, CellStateChangedEventArgs args)
    {
      if (args.IsAlive)
      {
        _game.SetAlive(args.Y, args.X);
      }
      else
      {
        _game.SetDead(args.Y, args.X);
      }
    }

    private void MainWindow_OnClosing(object sender, CancelEventArgs e)
    {
      if (_game != null)
      {
        _game.Stop();
      }
    }

    private void StepButtonClick(object sender, RoutedEventArgs e)
    {
      Step();
    }

    private void Step()
    {
      if (_game == null)
      {
        MessageBox.Show("Set game field");
        return;
      }

      IGameState gameState = _game.Step();
      GenerationTextBlock.Text = _game.CurrentGeneration.ToString();

      foreach (var cell in gameState.Differences)
      {
        _cells[cell.Y, cell.X].SetState(cell.Alive);
      }
    }
  }
}