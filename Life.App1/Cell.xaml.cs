﻿using System;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;

namespace Life.App1
{
  public partial class Cell : UserControl
  {
    public Cell()
    {
      InitializeComponent();

      Rectangle.Fill = Brushes.IndianRed;
      Rectangle.Stroke = Brushes.DarkBlue;
      Rectangle.StrokeThickness = 0.5;
    }

    public bool IsAlive { get; set; }
    public int X { get; set; }
    public int Y { get; set; }
    public event EventHandler<CellStateChangedEventArgs> CellStateChanged;

    public void SetState(bool isAlive)
    {
      IsAlive = isAlive;
      Rectangle.Fill = IsAlive ? Brushes.LawnGreen : Brushes.Transparent;
    }

    protected virtual void OnCellStateChanged(CellStateChangedEventArgs e)
    {
      EventHandler<CellStateChangedEventArgs> handler = CellStateChanged;
      if (handler != null)
      {
        handler(this, e);
      }
    }

    protected override void OnMouseLeftButtonDown(MouseButtonEventArgs e)
    {
      base.OnMouseLeftButtonDown(e);
      SetState(!IsAlive);
      OnCellStateChanged(new CellStateChangedEventArgs(Y, X, IsAlive));
    }
  }

  public class CellStateChangedEventArgs : EventArgs
  {
    public CellStateChangedEventArgs(int y, int x, bool isAlive)
    {
      X = x;
      Y = y;
      IsAlive = isAlive;
    }

    public int X { get; set; }
    public int Y { get; set; }
    public bool IsAlive { get; set; }
  }
}